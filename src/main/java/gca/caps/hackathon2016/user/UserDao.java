package gca.caps.hackathon2016.user;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.sql.DataSource;

@Component
public class UserDao {

	private JdbcTemplate jdbcTemplate;

    @Inject
    private UserMapper userMapper;

    @Inject
	public void init(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}


    public User findUser(String login, String password) {
        try {
            return jdbcTemplate.queryForObject("select * from user where login = ? and password = ?",
                    new Object[]{login, password}, userMapper);
        } catch (EmptyResultDataAccessException noRow) {
            return null;
        }
    }
}
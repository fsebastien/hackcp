package gca.caps.hackathon2016.user;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

@Configuration
@RestController
@SessionAttributes("user")
public class LoginController {

    @Inject
    UserDao userDao;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public boolean login(HttpSession session, @RequestBody User userForm) {
        User authenticatedUser = userDao.findUser(userForm.getLogin(), userForm.getPassword());
        session.setAttribute("user", authenticatedUser);
        return authenticatedUser != null;
    }

    @RequestMapping("/disconnect")
    public boolean disconnect(SessionStatus status){
        status.setComplete();
        return true;
    }

    @RequestMapping("/user")
    public User user2(@ModelAttribute("user") User user) {
        return user;
    }
}

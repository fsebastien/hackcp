package gca.caps.hackathon2016.operations;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.math.BigInteger;
import java.util.*;

@Repository
public class OperationDao {

    private JdbcTemplate jdbcTemplate;

    @Inject
    public void init(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Inject
    private OperationMapper operationMapper;

    //Methode qui retourne les montants groupes par category
    public Map<String, Long> sumOperationsAmountsBycategoryByAccount(String accountNumber) {
        Map<String, Long> result = new HashMap<String, Long>();
        List<Operation> operations = this.jdbcTemplate.query("select * from operation where account = ?",
                new Object[]{accountNumber},
                operationMapper);

        for (Operation op:operations) {
            //Si la categorie existe deja, on ajoute le montant a ce qui existe deja
            if ( result.containsKey(op.getCategory())) {
                result.replace(op.getCategory(), result.get(op.getCategory())+op.getAmount());
            } else {
                //Sinon on insere la nouvelle categorie et le montant
                result.put(op.getCategory(), op.getAmount());
            }
        }
        return result;
    }

    //Methode qui retourne les montants groupés par tiers
    public Map<String, Long>  sumOperationsAmountByThirdpartyByAccount(String accountNumber) {
        Map<String, Long> result = new HashMap<String, Long>();
        List<Operation> operations = this.jdbcTemplate.query("select * from operation where account = ?",
                new Object[]{accountNumber},
                operationMapper);

        for (Operation op:operations) {
            //Si le thirdparty existe deja, on ajoute le montant a ce qui existe deja
            if ( result.containsKey(op.getThirdparty())) {
                result.replace(op.getThirdparty(), result.get(op.getThirdparty())+op.getAmount());
            } else {
                //Sinon on insere le nouveau thirdparty et le montant
                result.put(op.getThirdparty(), op.getAmount());
            }
        }
        return result;

    }

    //Supprimé, ça compile pas a cause du type qui est un enum
    /*
    //Methode qui retourne les montants groupés par type
    public Map<String, Long>  sumOperationsAmountByTypeByAccount(String accountNumber) {
        Map<String, Long> result = new HashMap<String, Long>();
        List<Operation> operations = this.jdbcTemplate.query("select * from operation where account = ?",
                new Object[]{accountNumber},
                operationMapper);

        for (Operation op:operations) {
            //Si le thirdparty existe deja, on ajoute le montant a ce qui existe deja
            if ( result.containsKey(op.getType())) {
                result.replace(op.getType(), result.get(op.getType())+op.getAmount());
            } else {
                //Sinon on insere le nouveau thirdparty et le montant
                result.put(op.getType(), op.getAmount());
            }
        }
        return result;

    }
    */

    public Long sumOperationsAmountByAccount(String accountNumber) {
        Long result= Long.valueOf(0);
        List<Operation> operations = this.jdbcTemplate.query("select * from operation where account = ?",
                new Object[]{accountNumber},
                operationMapper);

        for (Operation op:operations) {
            result+=op.getAmount();
        }

        return result;

    }

    public Collection<Operation> findAllByAccount(String accountNumber) {
        return this.jdbcTemplate.query("select * from operation where account = ?",
                new Object[] {accountNumber}, operationMapper);

    }

    public void save(Operation operation) {
        jdbcTemplate.update("insert into operation (amount, name, date, account, thirdparty, category, status, type) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                operation.getAmount(),
                operation.getName(), 
                operation.getDate(),
                operation.getAccount().getNumber(),
                operation.getThirdparty(),
                operation.getCategory(),
                operation.getStatus(),
                operation.getType());
    }
}

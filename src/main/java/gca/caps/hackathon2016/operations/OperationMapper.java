package gca.caps.hackathon2016.operations;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Component
public class OperationMapper implements RowMapper<Operation> {
    @Override
    public Operation mapRow(ResultSet resultSet, int i) throws SQLException {
        Operation operation = new Operation();

        operation.setId(resultSet.getInt("id"));
        operation.setAmount(resultSet.getLong("amount"));
        operation.setName(resultSet.getString("name"));
        //operation.setDate(resultSet.getDate("date").toLocalDate());
        operation.setStatus(Operation.OperationStatus.valueOf(resultSet.getString("status")));
        operation.setType(Operation.OperationType.valueOf(resultSet.getString("type")));
        operation.setThirdparty(resultSet.getString("thirdparty"));
        operation.setCategory(resultSet.getString("category"));

        operation.setAccount(null);


        return operation;
    }
}

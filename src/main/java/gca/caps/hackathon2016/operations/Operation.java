package gca.caps.hackathon2016.operations;

import gca.caps.hackathon2016.account.Account;

import java.time.LocalDate;

public class Operation {

	private Integer id;
	private Long amount;
	private String name;
	private LocalDate date;
	private Account account;
	private OperationStatus status;
	private OperationType type;


	private String category;
	private String thirdparty;

	public enum OperationStatus {
		PENDING, RESOLVED;
	}

	public enum OperationType {
		TRANSFER, CARD, CHEQUE;
	}

	public static Builder newOperationBuilder() {
		return new Builder();
	}

	public static class Builder {

		private Operation operation = new Operation();

		public Builder withId(Integer id) {
			operation.id = id;
			return this;
		}

		public Builder withAmount(Long amount) {
			operation.amount = amount;
			return this;
		}

		public Builder withName(String name) {
			operation.name = name;
			return this;
		}

		public Builder withDate(LocalDate date) {
			operation.date = date;
			return this;
		}

		public Builder withAccount(Account account) {
			operation.account = account;
			return this;
		}

		public Builder withStatus(OperationStatus status) {
			operation.status = status;
			return this;
		}

		public Builder withType(OperationType type) {
			operation.type = type;
			return this;
		}

		public Builder withThirdparty(String thirdparty) {
			operation.thirdparty = thirdparty;
			return this;
		}
		public Operation build() {
			return operation;
		}

        public Builder withCategory(String category) {
            operation.category = category;
            return this;
        }
    }

	public OperationSign getSign() {
		return OperationSign.getSign(amount);
	}

    public enum OperationSign {

        CREDIT, DEBIT;

        public static OperationSign getSign(Long amount) {
            return isCredit(amount) ? CREDIT : DEBIT;
        }

        public static boolean isCredit(Long amount) {
            return amount == null || amount.compareTo(0L) >= 0;
        }
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((account == null) ? 0 : account.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Operation other = (Operation) obj;
		if (account == null) {
			if (other.account != null)
				return false;
		} else if (!account.equals(other.account))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		return true;
	}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public OperationStatus getStatus() {
        return status;
    }

    public void setStatus(OperationStatus status) {
        this.status = status;
    }

    public OperationType getType() {
        return type;
    }

    public void setType(OperationType type) {
        this.type = type;
    }
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getThirdparty() {
		return thirdparty;
	}

	public void setThirdparty(String thirdparty) {
		this.thirdparty = thirdparty;
	}
}

package gca.caps.hackathon2016.operations;

import gca.caps.hackathon2016.account.AccountDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.inject.Inject;
import java.util.Collection;

@RestController
@RequestMapping("/user/{user}/account/{accountNumber}/")
//@SessionAttributes({"user"})
public class OperationResource {

    private static final Logger logger = LoggerFactory.getLogger(OperationResource.class);

    @Inject
    private OperationDao operationDao;

    @Inject
    private AccountDao accountDao;

    @RequestMapping("/operations")
    public Collection<Operation> operations(@PathVariable String accountNumber) {

        // TODO check authorisation du user
        return operationDao.findAllByAccount(accountNumber);
    }
}

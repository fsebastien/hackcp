package gca.caps.hackathon2016.misc;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class StateMapper implements RowMapper<State> {

		@Override
		public State mapRow(ResultSet rs, int rowNum) throws SQLException {
			State s = new State();
			s.setId(rs.getLong("id"));
			s.setStateCode(rs.getString("state_code"));
			s.setName(rs.getString("name"));
			return s;
		}
	}
package gca.caps.hackathon2016.misc;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class EnvironmentController {

	private static final Logger logger = LoggerFactory.getLogger(EnvironmentController.class);

	@Inject
	private ReferenceDataRepository referenceRepository;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index() {
		return "index";
	}

	@RequestMapping(value = "/dbinfo", method = RequestMethod.GET)
	public String dbinfo(Model model) {
		logger.info("DB Information requested");
		model.addAttribute("dbinfo", referenceRepository.getDbInfo());
		model.addAttribute("states", referenceRepository.findAll());
		return "dbinfo";
	}

}

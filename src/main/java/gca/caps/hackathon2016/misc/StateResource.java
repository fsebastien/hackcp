package gca.caps.hackathon2016.misc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
public class StateResource {

    private static final Logger logger = LoggerFactory.getLogger(StateResource.class);

    public static final List<String> DEVELOPERS = Arrays.asList(
            "Florent, qu'est-ce que t'as encore pas fait ?",
            "Luc, t'as tout cassé",
            "Charles, lève la tête tu fais plein de bétises!",
            "Pascal, ce sont tes enigmes qui nous font perdre la tête!",
            "Samuel, arrête le café ça va pas là..");

    @Inject
    private ReferenceDataRepository referenceRepository;

    @RequestMapping("/state")
    public State state(@RequestParam(value="name", defaultValue="inconnu") String name) {
        return referenceRepository.findState(name);
    }

    @RequestMapping("/states")
    public Collection<State> states() {
        return referenceRepository.findAll();
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView exceptionHandler(HttpServletRequest req, Exception exception) {
        logger.error("Request: " + req.getRequestURL() + " raised " + exception);

        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", exception);
        mav.addObject("url", req.getRequestURL());
        mav.addObject("fausseRaison", selectRandomDeveloper());
        mav.setViewName("exception");
        return mav;
    }

    private Object selectRandomDeveloper() {
        return DEVELOPERS.get(new Random().nextInt(DEVELOPERS.size()));
    }
}

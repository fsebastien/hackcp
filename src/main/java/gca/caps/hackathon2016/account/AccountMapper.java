package gca.caps.hackathon2016.account;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

@Component
public class AccountMapper implements RowMapper<Account> {
    @Override
    public Account mapRow(ResultSet resultSet, int i) throws SQLException {
        Account account = new Account();
        account.setNumber(resultSet.getString("number"));
        account.setBalance(resultSet.getLong("balance"));
        //LocalDate balancedate = resultSet.getDate("balancedate").toLocalDate();
        //account.setBalanceDate(balancedate);
        account.setType(Account.AccountType.valueOf(resultSet.getString("type")));

        account.setUser(null);

        return account;
    }
}

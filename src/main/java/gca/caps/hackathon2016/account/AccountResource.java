package gca.caps.hackathon2016.account;

import gca.caps.hackathon2016.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.Collection;

@RestController
//@SessionAttributes({"user"})
public class AccountResource {

    private static final Logger logger = LoggerFactory.getLogger(AccountResource.class);

    @Inject
    private AccountDao accountDao;

    @RequestMapping("/user/{user}/account/{accountNumber}")
    public Account account(@PathVariable("user") String user,
                           @PathVariable("accountNumber") String accountNumber) {
        return accountDao.findAccountByNumber(accountNumber);
    }

    @RequestMapping(value = "/user/{user}/accounts", method = RequestMethod.GET)
    public Collection<Account> listAccounts(@PathVariable("user") String user) {
        return accountDao.findAccountsByUser(user);
    }

    @RequestMapping(value = "/accounts/all", method = RequestMethod.GET)
    public Collection<Account> listAllAccounts() {
        return accountDao.findAllAccounts();
    }

    @RequestMapping(value = "/user/{user}/accounts", method = RequestMethod.POST)
    ResponseEntity<?> create(@RequestBody Account input, @PathVariable("user") String user) {

        input.setUser(user);

        accountDao.persistAccount(input);

        HttpHeaders httpHeaders = new HttpHeaders();
        return new ResponseEntity<>(null, httpHeaders, HttpStatus.CREATED);
    }




}

package gca.caps.hackathon2016.account;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.util.Collection;

@Repository
public class AccountDao {

    private JdbcTemplate jdbcTemplate;

    @Inject
    public void init(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Inject
    private AccountMapper accountMapper;

    public Collection<Account> findAccountsByUser(String user) {
        return this.jdbcTemplate.query("select * from account where user = ?",
                new Object[] {user}, accountMapper);
    }

    public Account findAccountByNumber(String accountNumber) {
        try {
            return this.jdbcTemplate.queryForObject("select * from account where number = ?",
                    new Object[]{accountNumber}, accountMapper);
        } catch (EmptyResultDataAccessException exception) {
            return null;
        }
    }

    public void persistAccount(Account account) {
        jdbcTemplate.update("insert into account (number, balance, balancedate, type, user) VALUES (?, ?, ?, ?, ?)",
                account.getNumber(), account.getBalance(), account.getBalanceDate(), account.getType(), account.getUser());
    }

    public void updateAccount(Account account) {
        jdbcTemplate.update("update account set balance=?, balancedate=?, type=?, user=? where number=?",
                account.getBalance(), account.getBalanceDate(), account.getType(), account.getUser(), account.getNumber());
    }

    public Collection<Account> findAllAccounts() {
        return this.jdbcTemplate.query("select * from account", accountMapper);
    }
}

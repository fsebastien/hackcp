package gca.caps.hackathon2016.transfer;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

public class TransferCommand implements Serializable {

	private static final long serialVersionUID = -1733253994577062650L;

	@NotNull
	private String debitedAccountNumber;

	@NotNull
	private String creditedAccountNumber;

	@Min(10)
	private Long amount;

	public String getDebitedAccountNumber() {
		return debitedAccountNumber;
	}

	public String getCreditedAccountNumber() {
		return creditedAccountNumber;
	}

	public Long getAmount() {
		return amount;
	}

	public void setDebitedAccountNumber(String debitedAccountNumber) {
		this.debitedAccountNumber = debitedAccountNumber;
	}

	public void setCreditedAccountNumber(String creditedAccountNumber) {
		this.creditedAccountNumber = creditedAccountNumber;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "TransferCommand{" +
				"debitedAccountNumber='" + debitedAccountNumber + '\'' +
				", creditedAccountNumber='" + creditedAccountNumber + '\'' +
				", amount=" + amount +
				'}';
	}
}

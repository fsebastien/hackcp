package gca.caps.hackathon2016.transfer;

import gca.caps.hackathon2016.account.Account;
import gca.caps.hackathon2016.account.AccountDao;
import gca.caps.hackathon2016.operations.Operation;
import gca.caps.hackathon2016.operations.OperationDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.inject.Inject;
import java.time.LocalDate;

import static gca.caps.hackathon2016.operations.Operation.OperationType.TRANSFER;

@Service
@Transactional(readOnly = true)
public class TransferService {

    @Inject
    private AccountDao accountDao;

    @Inject
    private OperationDao operationDao;

    @Transactional(readOnly = false)
    public void performTransfer(String debitedAccountId, String creditedAccountId, Long amount)
            throws UnsufficientBalanceException {

        Account debitedAccount = accountDao.findAccountByNumber(debitedAccountId);
        Assert.notNull(debitedAccount, "unknown account");

        if (debitedAccount.getBalance().compareTo(amount) < 0) {
            throw new UnsufficientBalanceException();
        }

        Account creditedAccount = accountDao.findAccountByNumber(creditedAccountId);
        Assert.notNull(creditedAccount, "unknown account");

        debitedAccount.setBalance(debitedAccount.getBalance() - amount);
        creditedAccount.setBalance(creditedAccount.getBalance() + amount);

        LocalDate now = LocalDate.now();
        Operation.OperationStatus status = Operation.OperationStatus.RESOLVED;
        debitedAccount.setBalanceDate(now);
        creditedAccount.setBalanceDate(now);

        //construction de deux opérations: une de débit, et une de crédit
        Operation debitOperation = new Operation.Builder().withName("transfert -" + amount).withAccount(debitedAccount).withAmount(- amount).withDate(now).withThirdparty(creditedAccount.getNumber())
                .withStatus(status).withCategory("Loisir").withType(TRANSFER).build();
        Operation creditOperation = new Operation.Builder().withName("transfert +" + amount).withAccount(creditedAccount).withAmount(amount).withDate(now).withThirdparty(debitedAccount.getNumber())
                .withStatus(status).withCategory("Loisir").withType(TRANSFER).build();

        //Mise à jour des soldes des deux comptes
        accountDao.updateAccount(debitedAccount);
        accountDao.updateAccount(creditedAccount);

        //Insertion des opérations
        operationDao.save(debitOperation);
        operationDao.save(creditOperation);
    }
}

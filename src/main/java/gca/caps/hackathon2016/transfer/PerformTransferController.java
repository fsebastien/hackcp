package gca.caps.hackathon2016.transfer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;

@RestController
public class PerformTransferController {

    @Inject
    private TransferService transferService;

    private Logger logger = LoggerFactory.getLogger(PerformTransferController.class);

    @RequestMapping(value = "/user/{user}/account/{accountNumber}/transfer", method = RequestMethod.POST)
    public boolean performTransfer(@PathVariable("user") String user,
                                   @PathVariable String accountNumber,
                                   @RequestBody TransferCommand command) {

        logger.info("Initialisation du transfert de {}", command);
        try {
            transferService.performTransfer(command.getDebitedAccountNumber(),
                    command.getCreditedAccountNumber(), command.getAmount());

            return true;

        } catch (UnsufficientBalanceException e) {
            logger.info("insufficient amount");
            return false;
        }
    }
}

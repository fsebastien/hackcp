-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 192.168.0.70:3306
-- Generation Time: May 19, 2016 at 04:23 AM
-- Server version: 5.5.47-0ubuntu0.12.04.1-log
-- PHP Version: 5.5.32-1+deb.sury.org~precise+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dff36cf1575ac4e828ee72dbe522eca90`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
  `number` varchar(34) NOT NULL,
  `balance` bigint(20) NOT NULL,
  `balancedate` date NOT NULL,
  `type` varchar(20) NOT NULL,
  `user` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`number`, `balance`, `balancedate`, `type`, `user`) VALUES
('FR01344356765333888A01', 123, '2001-01-16', 'PERSONNAL_CHECKING', 'azeblouse'),
('FR01344356765333888A02', 456, '2001-01-16', 'PERSONNAL_CHECKING', 'craton'),
('FR01344356765333888A03', 789, '2001-01-16', 'PERSONNAL_CHECKING', 'gmauve'),
('FR01344356765333888A04', 12300, '2001-01-16', 'PERSONNAL_CHECKING', 'massin');

-- --------------------------------------------------------

--
-- Table structure for table `operation`
--

DROP TABLE IF EXISTS `operation`;
CREATE TABLE `operation` (
  `id` int(11) NOT NULL,
  `amount` bigint(20) NOT NULL,
  `name` longtext NOT NULL,
  `date` date NOT NULL,
  `account` varchar(34) NOT NULL,
  `thirdparty` varchar(100) DEFAULT NULL,
  `category` varchar(100) NOT NULL,
  `status` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `operation`
--

INSERT INTO `operation` (`id`, `amount`, `name`, `date`, `account`, `thirdparty`, `category`, `status`, `type`) VALUES
(2, 10000, 'Salaire CA Février 16', '2025-02-16', 'FR01344356765333888A01', 'Crédit Agricole', 'Revenu', 'RESOLVED', 'TRANSFER'),
(3, 10000, 'Salaire CA Mars 16', '2025-03-16', 'FR01344356765333888A01', 'Crédit Agricole', 'Revenu', 'RESOLVED', 'TRANSFER'),
(4, 10000, 'Salaire CA Avril 16', '2025-04-16', 'FR01344356765333888A01', 'Crédit Agricole', 'Revenu', 'RESOLVED', 'TRANSFER'),
(5, 10000, 'Salaire CA Mai 16', '2025-05-16', 'FR01344356765333888A01', 'Crédit Agricole', 'Revenu', 'RESOLVED', 'TRANSFER'),
(6, -624, 'Abonnement', '2001-01-16', 'FR01344356765333888A01', 'EDF', 'Habitation', 'RESOLVED', 'TRANSFER'),
(7, -52, 'Navigo', '2002-01-16', 'FR01344356765333888A01', 'RATP', 'Vie courante', 'RESOLVED', 'TRANSFER'),
(8, -123, 'Remplacement genoux', '2002-01-16', 'FR01344356765333888A01', 'Clinique Agricole', 'Santé', 'RESOLVED', 'TRANSFER'),
(9, -4300, 'Hotel Martinez', '2003-02-16', 'FR01344356765333888A01', 'MARTINEZ', 'Loisir', 'RESOLVED', 'TRANSFER'),
(10, -3234, 'Vol Paris-Bali', '2004-03-16', 'FR01344356765333888A01', 'AIR FRANCE', 'Loisir', 'RESOLVED', 'TRANSFER'),
(11, -2524, 'A/R Monreal-Venise', '2005-04-16', 'FR01344356765333888A01', 'AIR DIAMOND', 'Loisir', 'RESOLVED', 'TRANSFER'),
(12, 2500, 'Remboursement de Teddy', '2001-05-16', 'FR01344356765333888A01', 'Teddy', 'Loisir', 'RESOLVED', 'TRANSFER'),
(13, 9000, 'Salaire TOTAL Janvier 2016', '2025-01-16', 'FR01344356765333888A02', 'TOTAL', 'Revenu', 'RESOLVED', 'TRANSFER'),
(14, 9000, 'Salaire TOTAL Février 2016', '2025-02-16', 'FR01344356765333888A02', 'TOTAL', 'Revenu', 'RESOLVED', 'TRANSFER'),
(15, 9000, 'Salaire TOTAL Mars 2016', '2025-03-16', 'FR01344356765333888A02', 'TOTAL', 'Revenu', 'RESOLVED', 'TRANSFER'),
(16, 9000, 'Salaire TOTAL Avril 2016', '2025-04-16', 'FR01344356765333888A02', 'TOTAL', 'Revenu', 'RESOLVED', 'TRANSFER'),
(17, 9000, 'Salaire TOTAL Mai 2016', '2025-05-16', 'FR01344356765333888A02', 'TOTAL', 'Revenu', 'RESOLVED', 'TRANSFER'),
(18, -56, 'Courses', '2001-03-16', 'FR01344356765333888A02', 'CARROUF', 'Vie courante', 'RESOLVED', 'TRANSFER'),
(19, -234, 'Stock EPO', '2005-04-16', 'FR01344356765333888A02', 'EPO Store', 'Santé', 'RESOLVED', 'TRANSFER'),
(20, -156, 'Réglage vue', '2005-02-16', 'FR01344356765333888A02', 'EYES boutique', 'Santé', 'RESOLVED', 'TRANSFER'),
(21, -100, 'Sport : Inscription BTR2014', '2025-05-16', 'FR01344356765333888A02', 'Chilkoot', 'Loisir', 'RESOLVED', 'TRANSFER'),
(22, -200, 'Paris-Brest-Paris', '2025-05-16', 'FR01344356765333888A02', 'FDFR', 'Loisir', 'RESOLVED', 'TRANSFER'),
(23, -343, 'Hotel Le Magnifique', '2025-05-16', 'FR01344356765333888A02', 'La Magnifique', 'Vie courante', 'RESOLVED', 'TRANSFER'),
(24, -656, 'Location Renault Sport', '2025-05-16', 'FR01344356765333888A02', 'RENAULT', 'Loisir', 'RESOLVED', 'TRANSFER'),
(25, -25, 'Matos rando', '2025-05-16', 'FR01344356765333888A02', 'Vieux Campeur', 'Loisir', 'RESOLVED', 'TRANSFER'),
(26, -12, 'Jeu direction', '2025-05-16', 'FR01344356765333888A02', 'Alltricks', 'Loisir', 'RESOLVED', 'TRANSFER'),
(27, 10000, 'Salaire CA Janvier 16', '2025-01-16', 'FR01344356765333888A03', 'Crédit Agricole', 'Revenu', 'RESOLVED', 'TRANSFER'),
(28, 10000, 'Salaire CA Février 16', '2025-02-16', 'FR01344356765333888A03', 'Crédit Agricole', 'Revenu', 'RESOLVED', 'TRANSFER'),
(29, 10000, 'Salaire CA Mars 16', '2025-03-16', 'FR01344356765333888A03', 'Crédit Agricole', 'Revenu', 'RESOLVED', 'TRANSFER'),
(30, 10000, 'Salaire CA Avril 16', '2025-04-16', 'FR01344356765333888A03', 'Crédit Agricole', 'Revenu', 'RESOLVED', 'TRANSFER'),
(31, 10000, 'Salaire CA Mai 16', '2025-05-16', 'FR01344356765333888A03', 'Crédit Agricole', 'Revenu', 'RESOLVED', 'TRANSFER'),
(32, -45, 'BEBE STORE Stock de couches', '2001-01-16', 'FR01344356765333888A03', 'BEBE STORE', 'Vie courante', 'RESOLVED', 'TRANSFER'),
(33, -78, 'Habits', '2002-01-16', 'FR01344356765333888A03', 'MOME Market', 'Vie courante', 'RESOLVED', 'TRANSFER'),
(34, -15, 'Pack chatouilles', '2003-01-16', 'FR01344356765333888A03', 'Poulbot Store', 'Vie courante', 'RESOLVED', 'TRANSFER'),
(35, -234, 'Valise bisous', '2004-01-16', 'FR01344356765333888A03', 'Poulette Boutique', 'Vie courante', 'RESOLVED', 'TRANSFER'),
(36, -524, 'JUNIOR – fournitures geek', '2005-01-16', 'FR01344356765333888A03', 'LDLC', 'Loisirs', 'RESOLVED', 'TRANSFER'),
(37, -129, 'Pitchoun, appareil dentaire', '2001-01-16', 'FR01344356765333888A03', 'Tooth SARL', 'Santé', 'RESOLVED', 'TRANSFER'),
(38, 4224, 'Salaire Janvier 2016', '2025-01-16', 'FR01344356765333888A04', 'PEUGEOT', 'Revenu', 'RESOLVED', 'TRANSFER'),
(39, 4224, 'Salaire Février 2016', '2025-02-16', 'FR01344356765333888A04', 'PEUGEOT', 'Revenu', 'RESOLVED', 'TRANSFER'),
(40, 4224, 'Salaire Mars 2016', '2025-03-16', 'FR01344356765333888A04', 'PEUGEOT', 'Revenu', 'RESOLVED', 'TRANSFER'),
(41, 4224, 'Salaire Avril 2016', '2025-04-16', 'FR01344356765333888A04', 'PEUGEOT', 'Revenu', 'RESOLVED', 'TRANSFER'),
(42, 4224, 'Salaire Mai 2016', '2025-05-16', 'FR01344356765333888A04', 'PEUGEOT', 'Revenu', 'RESOLVED', 'TRANSFER'),
(43, -1289, 'Paris-Vladivostok', '2012-03-16', 'FR01344356765333888A04', 'Air France', 'Loisir', 'RESOLVED', 'TRANSFER'),
(44, -67, 'Courses', '2004-03-16', 'FR01344356765333888A04', 'Carrouf', 'Vie courante', 'RESOLVED', 'TRANSFER'),
(45, -567, 'Implants', '2002-03-16', 'FR01344356765333888A04', 'La jardinnerie', 'Santé', 'RESOLVED', 'TRANSFER'),
(46, -456, 'Musée Tokyo', '2002-02-16', 'FR01344356765333888A04', 'Asie Dream', 'Loisir', 'RESOLVED', 'TRANSFER'),
(47, -700, 'Ravitaillement', '2004-01-16', 'FR01344356765333888A04', 'Monop', 'Vie courante', 'RESOLVED', 'TRANSFER'),
(48, -782, 'Raid Sahara', '2005-02-16', 'FR01344356765333888A04', 'Afrique Corp', 'Loisir', 'RESOLVED', 'TRANSFER');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `login` varchar(20) NOT NULL,
  `firstname` varchar(40) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `password` varchar(80) NOT NULL,
  `email` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`login`, `firstname`, `lastname`, `password`, `email`) VALUES
('azeblouse', 'Agathe', 'Zeblouse', 'v001', 'Agathe.zeblouse@gmail.com'),
('ckayser', 'Charles', 'Kayser', 'x', 'kayser.charley@gmail.com'),
('craton', 'Candy', 'Raton', 'w002', 'Candy.raton@free.fr'),
('gmauve', 'Guy', 'Mauve', 'x003', 'Guy.mauve@orange.fr'),
('massin', 'Marc', 'Assin', 'y004', 'Marc.assin@hotmail.fr');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`number`),
  ADD UNIQUE KEY `number` (`number`);

--
-- Indexes for table `operation`
--
ALTER TABLE `operation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`login`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `operation`
--
ALTER TABLE `operation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

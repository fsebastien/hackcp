var filters = angular.module('app.filters', []);

filters.filter('arValue', function() {
    return function(input) {
        return input=="A" ? 'Acquisition':'Restitution';
    };
});
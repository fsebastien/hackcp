var services = angular.module('app.services', ['ngResource']);


services.factory('StateService', function ($resource) {
    return $resource('state', {}, {
        list:      {method: 'GET', isArray: true, url: "states"},
/*
        create:     {method: 'POST'},
        update:     {method: 'POST', url: 'mxamo/resource/pilotage/exclusionRules/update'},
*/
//        get:       {method: 'GET',  url: 'state/:name' }
/*
        remove:     {method: 'POST', url: 'mxamo/resource/pilotage/exclusionRules/:id', params: {id: '@id'}},
        activer:    {method: 'POST', url: 'mxamo/resource/pilotage/exclusionRules/:id/activer', params: {id: '@id'}},
        desactiver: {method: 'POST', url: 'mxamo/resource/pilotage/exclusionRules/:id/desactiver', params: {id: '@id'}},
        isUnique:   {method: 'POST', url: 'mxamo/resource/pilotage/exclusionRules/nameUnique?id=:id', params: {id: '@id'}}
*/
    })
});
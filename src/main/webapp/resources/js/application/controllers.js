var controllers = angular.module('app.controllers', []);

controllers.controller('MainController',
    function ($scope, $http, $interval) {
        $scope.loading = true;

    });

controllers.controller('ListStateController', ['$scope', '$http', '$log', 'StateService',
    function ($scope, $http, $log, StateService) {
        $scope.loading = true;
        
        refresh();
        // action bouton Rafraichir
        $scope.refresh = refresh;
        function refresh() {
            $scope.states = StateService.list();
        }
        
    }]);

controllers.controller('ViewStateController', ['$scope', '$http', '$log', '$routeParams', 'StateService',
    function ($scope, $http, $log, $routeParams, StateService) {
        $scope.loading = true;

        refresh();

        // action bouton Rafraichir
        $scope.refresh = refresh;
        function refresh() {
            $scope.state = StateService.get({name:$routeParams.name});
        }

    }]);
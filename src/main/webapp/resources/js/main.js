var acqboApp = angular.module('hacaps', ['app.controllers', 'app.services', 'app.directives', 'app.filters', 'ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
            when("/dbinfo", {
                templateUrl: 'dbinfo'
            }).
            when('/states', {
                templateUrl:'resources/partials/list-state.html',
                controller:'ListStateController'}).
            when('/state/:name', {
                templateUrl:'resources/partials/view-state.html',
                controller:'ViewStateController'}).
            otherwise({redirectTo: '/'});
    }]);

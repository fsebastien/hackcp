<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<body>
<h1>Oops..</h1>
<p>Une erreur applicative s'est produite</p>
<p>${fausseRaison}</p>

    <p>Failed URL: ${url}</p>
    <p>Exception:  ${exception.message}</p>
    <p>
    <c:forEach items="${exception.stackTrace}" var="ste">
        <span>${ste}</span>
    </c:forEach>
    </p>

</body>
</html>
<!DOCTYPE html>
<html ng-app="hacaps" lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="SHORTCUT ICON" href="resources/img/logo_ca.gif"/>
    <title>HACAPS - Eq.3 - Application Hackaton CA Payments and Services</title>
    <link rel="stylesheet" href="resources/bootstrap-3.3.5/css/bootstrap.min.css">
    <!--<link rel="stylesheet" href="resources/css/AdminLTE.css">-->
    <link rel="stylesheet" href="resources/css/main.css">
    <!-- Custom styles for this template -->
    <link href="resources/css/sticky-footer-navbar.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body ng-controller="MainController">

<nav class="navbar navbar-default navbar-static-top">
    <div class="header">
        <div class="container">

            <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
                <div class="navbar-header">
                    <img alt="Logo CA-CP" src="resources/img/logo-cacp.png">
                </div>
                <ul class="nav navbar-nav navbar-right" data-deferred-cloak>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;{{user.prenom}}
                            {{user.nom}}</a>

                        <ul class="dropdown-menu dropdown-info" role="menu">
                            <li>Profils
                                <ul>
                                    <li ng-repeat="profile in profiles">{{profile}}</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>

    </div>
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                    aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span> <span
                    class="icon-bar"></span> <span class="icon-bar"></span> <span
                    class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Equipe 3 CA-CPS </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        Environnement
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#/dbinfo">Database information</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        Etats américains
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#/states">Tous les états américains</a></li>
                        <li><a href="#/state/Maine">Maine</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div ng-view class="container">

</div>

<footer class="footer">
    <div class="container">
        <p class="text-muted">ACQ <span data-deferred-cloak>v{{version}}</span> | &nbsp;&copy; Crédit Agricole Cards &
            Services 2016</p>
    </div>
</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="resources/js/jquery/jquery-2.1.1.min.js"></script>
<script src="resources/bootstrap-3.3.5/js/bootstrap.min.js"></script>
<script src="webjars/angularjs/1.4.7/angular.min.js"></script>
<script src="webjars/angularjs/1.4.7/angular-route.js"></script>
<script src="webjars/angularjs/1.4.7/angular-resource.js"></script>

<!-- Application JS -->
<script src="resources/js/main.js"></script>
<script src="resources/js/application/controllers.js"></script>
<script src="resources/js/application/resources.js"></script>
<script src="resources/js/application/directives.js"></script>
<script src="resources/js/application/filters.js"></script>
</body>
</html>
****** Application de gestion de compte    ******
****** Credit Agricole - Payments Services ******
****** Hackathon    17-18-19 mai 2016      ******
****** Application : bankApp3.hdp.silca    ******


L'application réalise les fonctions suivantes :		
	[non] : Un utilisateur peut créer 2 (plusieurs) comptes	- Obligatoire
	[non] : Un utilisateur peut être connecté en sécurité à son compte	- Facultatif
	[oui] : Un utilisateur peut se connecter à un compte existant et voir son statut - Obligatoire
	[oui] : Un utilisateur peut naviguer via un menu dans toutes les fonctionnalités - Obligatoire
	[non] : Un utilisateur peut acheter une carte cadeau - Facultatif
	[oui] : Un utilisateur peut réaliser des transactions - Obligatoire
	[non] : Un utilisateur peut consulter des statistiques sur son compte -	Facultatif
	[oui] : Un utilisateur peut consulter l'historique de ses transactions - Facultatif
	[oui] : Les données sont stockées en base - Obligatoire

Fonctions ajoutées à l'énoncé de départ :
	[oui] : Lors d'un virementment, l'application vérifie que le montant de l'opération est inférieur ou égal au solde du compte du débiteur
	[oui] : L'application permet de créer plusieurs comptes bancaires pour un même utilisateur
	
	
3 composants gérés sur la plateforme STACKATO :
	1 composant Angular JS : gestion des IHM
	1 composant Java Spring Framework : Services
	1 composant Base de données : Mysql
	
Sources disponibles dans STACKATO
	
Données de tests :
	Jeux de données dans : src/test/resources/Testing
	Scénaris de tests danns : src/test/resources/Testing
	
	